import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Dmitry on 1/30/2016.
 */
public class Child2 extends ParentObject {

    String childField3;
    String childField4;

    public Child2(String field1, String field2, String childField3, String childField4) {
        super(field1, field2);
        this.childField3 = childField3;
        this.childField4 = childField4;
    }

    public String getChildField3() {
        return childField3;
    }

    public String getChildField4() {
        return childField4;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject result = super.toJSON();
        result.put("childField3_name", childField3);
        result.put("childField4_name", childField4);
        return result;
    }

}
