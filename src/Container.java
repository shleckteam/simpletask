import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitry on 1/30/2016.
 */
public class Container implements JSONSerializabel {

    private List<JSONSerializabel> someObjectList = new ArrayList<JSONSerializabel>();

    public Container() {
        someObjectList.add(new Child1("1","2","3","4"));
        someObjectList.add(new Child2("5","6","7","8"));
        someObjectList.add(new ParentObject("9","10"));
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONArray result = new JSONArray();
        Integer index = 0;
        for (JSONSerializabel object : someObjectList) {
            result.put(index, object.toJSON());
            index++;
        }
        return new JSONObject().put("values", result);
    }
}