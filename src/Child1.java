import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Dmitry on 1/30/2016.
 */
public class Child1 extends ParentObject {

    String childField1;
    String childField2;

    public Child1(String field1, String field2, String childField1, String childField2) {
        super(field1, field2);
        this.childField1 = childField1;
        this.childField2 = childField2;
    }

    public String getChildField1() {
        return childField1;
    }

    public String getChildField2() {
        return childField2;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject result = super.toJSON();
        result.put("childField1_name", childField1);
        result.put("childField2_name", childField2);
        return result;
    }

}
