import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Dmitry on 1/30/2016.
 */
public interface JSONSerializabel {

    public JSONObject toJSON() throws JSONException;

}