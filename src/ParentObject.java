import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Dmitry on 1/30/2016.
 */
public class ParentObject implements JSONSerializabel{

    String field1;
    String field2;

    public ParentObject(String field1, String field2) {
        this.field1 = field1;
        this.field2 = field2;
    }

    public String getField1() {
        return field1;
    }

    public String getField2() {
        return field2;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject result = new JSONObject();
        result.put("field1_name", field1);
        result.put("field2_name", field1);
        return result;
    }
}
